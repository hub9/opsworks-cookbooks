include_recipe "::install_code"

app = search(:aws_opsworks_app).first
app_path = "/srv/#{app['shortname']}"

systemd_file = "[Unit]\n"
systemd_file += "Description=nodejs server daemon\n\n"
systemd_file += "[Service]\n"
systemd_file += "User=root\n"
systemd_file += "Group=root\n"
systemd_file += "WorkingDirectory=#{app_path}/repo\n"
systemd_file += "ExecStart=/usr/bin/npm start\n"
systemd_file += "ExecReload=/bin/kill -s HUP $MAINPID\n"
systemd_file += "ExecStop=/bin/kill -s TERM $MAINPID\n"
systemd_file += "StandardOutput=syslog\n"
systemd_file += "StandardError=syslog\n"
systemd_file += "SyslogIdentifier=nodeapp-service\n"
systemd_file += "Restart=always\n\n"
systemd_file += "[Install]\n"
systemd_file += "WantedBy=multi-user.target"

file "/lib/systemd/system/nodeapp.service" do
  content systemd_file
  mode '0644'
  owner 'root'
  group 'root'
end

syslog_file = "if $programname == 'nodeapp-service' then /var/log/nodeapp_service.log\n"
syslog_file += "if $programname == 'nodeapp-service' then ~\n"

file "/etc/rsyslog.d/nodeapp_service.conf" do
  content syslog_file
  mode '0644'
  owner 'root'
  group 'root'
end

execute 'execute_systemd-reload-settings' do
  command "systemctl daemon-reload"
end

execute 'execute_systemd-enable-nodeapp' do
  command "systemctl enable nodeapp.service"
end

execute 'execute_systemd-start-nodeapp' do
  command "systemctl start nodeapp.service"
end

execute 'execute_systemd-restart-rsyslog' do
  command "systemctl restart rsyslog"
end

execute 'execute_systemd-restart-nodeapp' do
  command "systemctl restart nodeapp.service"
end

