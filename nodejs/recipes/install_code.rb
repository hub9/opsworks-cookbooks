app = search(:aws_opsworks_app).first
app_path = "/srv/#{app['shortname']}"
key_file = "/srv/#{app['shortname']}/keyfile"

directory app_path do
  owner 'root'
  group 'root'
  mode '0777'
  action :create
end

execute 'execute_gen_key' do
  cwd "#{app_path}/"
  command "echo \"#{app['app_source']['ssh_key']}\" > #{key_file}"
end

execute 'execute_chmod_key' do
  cwd "#{app_path}/"
  command "chmod 700 #{key_file}"
end

execute 'execute_clear_repo_folder' do
  cwd "#{app_path}/"
  command "rm -rf repo"
end

execute 'execute_clone_git' do
  cwd "#{app_path}/"
  command "ssh-agent bash -c 'ssh-add #{key_file}; git clone #{app['app_source']['url']} repo'"
end

execute 'chmod_allow_all_repo_folder' do
  cwd "#{app_path}/"
  command "chmod -R 777 repo"
end

execute 'execute_requirements' do
  cwd "#{app_path}/repo/"
  command "npm set user 0; npm set unsafe-perm true; npm install"
end

env_content = ""
app['environment'].each {|key, value| env_content += "#{key}=#{value}\n" }

file "#{app_path}/repo/.env" do
  content env_content
  mode '0777'
  owner 'root'
  group 'root'
end

