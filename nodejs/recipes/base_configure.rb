apps_path = "/srv/"

execute "apt-get update" do
  command "apt-get update"
end

package "git" do
  # workaround for:
  # WARNING: The following packages cannot be authenticated!
  # liberror-perl
  # STDERR: E: There are problems and -y was used without --force-yes
  options "--force-yes" if node["platform"] == "ubuntu" && node["platform_version"] == "14.04"
end

execute 'add_newer_nodejs_to_apt' do
  command "curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -"
end

package 'build-essential'
package 'nodejs'
package 'libappindicator1' 
package 'fonts-liberation'
package 'libasound2'
package 'libgconf-2-4'
package 'libnspr4'
package 'libnss3'
package 'libxss1'
package 'xdg-utils'

execute 'download_chrome' do
  cwd "#{apps_path}/"
  command "wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"
end

execute 'install_chrome' do
  cwd "#{apps_path}/"
  command "sudo dpkg -i google-chrome*.deb"
end

execute 'execute_add_remote_host' do
  command "ssh-keyscan github.com >> ~/.ssh/known_hosts"
end

execute 'execute_clear_apps_path' do
  cwd "#{apps_path}/"
  command "rm -rf *"
end
