include_recipe "::install_code"

app = search(:aws_opsworks_app).first
app_path = "/srv/#{app['shortname']}"

execute 'execute_requirements' do
  cwd "#{app_path}/repo/"
  command "pip3 install gunicorn"
end

gunicorn_conf_file = "import multiprocessing\n\n"
gunicorn_conf_file += "bind = '0.0.0.0:80'\n"
gunicorn_conf_file += "workers = multiprocessing.cpu_count() * 2 + 1\n"

file "#{app_path}/repo/gunicorn.conf.py" do
  content gunicorn_conf_file
  mode '0777'
  owner 'root'
  group 'root'
end

systemd_file = "[Unit]\n"
systemd_file += "Description=gunicorn daemon\n\n"
systemd_file += "[Service]\n"
systemd_file += "PIDFile=/run/gunicord/pid\n"
systemd_file += "User=root\n"
systemd_file += "Group=root\n"
systemd_file += "RuntimeDirectory=gunicorn\n"
systemd_file += "WorkingDirectory=#{app_path}/repo\n"
systemd_file += "ExecStart=/usr/local/bin/gunicorn -c #{app_path}/repo/gunicorn.conf.py --pid /run/gunicorn/pid webapp.wsgi:application\n"
systemd_file += "ExecReload=/bin/kill -s HUP $MAINPID\n"
systemd_file += "ExecStop=/bin/kill -s TERM $MAINPID\n"
systemd_file += "StandardOutput=syslog\n"
systemd_file += "StandardError=syslog\n"
systemd_file += "SyslogIdentifier=gunicorn-service\n"
systemd_file += "Restart=always\n\n"
systemd_file += "[Install]\n"
systemd_file += "WantedBy=multi-user.target"

file "/lib/systemd/system/gunicorn.service" do
  content systemd_file
  mode '0644'
  owner 'root'
  group 'root'
end

execute 'execute_systemd-reload-settings' do
  command "systemctl daemon-reload"
end

execute 'execute_systemd-enable-gunicorn' do
  command "systemctl enable gunicorn.service"
end

execute 'execute_systemd-start-gunicorn' do
  command "systemctl start gunicorn.service"
end

execute 'execute_systemd-restart-gunicorn' do
  command "systemctl restart gunicorn.service"
end
