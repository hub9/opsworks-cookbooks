include_recipe "::install_code"

app = search(:aws_opsworks_app).first
app_path = "/srv/#{app['shortname']}"

execute 'execute_migrate' do
  cwd "#{app_path}/repo/"
  command "python3 manage.py migrate"
end

systemd_file = "[Unit]\n"
systemd_file += "Description=celery beat daemon\n\n"
systemd_file += "[Service]\n"
systemd_file += "PIDFile=/run/celery_beat/pid\n"
systemd_file += "User=root\n"
systemd_file += "Group=root\n"
systemd_file += "RuntimeDirectory=celery_beat\n"
systemd_file += "WorkingDirectory=#{app_path}/repo\n"
systemd_file += "ExecStart=/usr/local/bin/celery beat -A webapp --pidfile=/run/celery_beat/pid --loglevel=info\n"
systemd_file += "ExecReload=/bin/kill -s HUP $MAINPID\n"
systemd_file += "ExecStop=/bin/kill -s TERM $MAINPID\n"
systemd_file += "StandardOutput=syslog\n"
systemd_file += "StandardError=syslog\n"
systemd_file += "SyslogIdentifier=celery-beat-service\n"
systemd_file += "Restart=always\n\n"
systemd_file += "[Install]\n"
systemd_file += "WantedBy=multi-user.target"

file "/lib/systemd/system/celery_beat.service" do
  content systemd_file
  mode '0644'
  owner 'root'
  group 'root'
end

execute 'execute_systemd-reload-settings' do
  command "systemctl daemon-reload"
end

execute 'execute_systemd-enable-celery-beat' do
  command "systemctl enable celery_beat.service"
end

execute 'execute_systemd-start-celery-beat' do
  command "systemctl start celery_beat.service"
end

execute 'execute_systemd-restart-celery-beat' do
  command "systemctl restart celery_beat.service"
end
