apps_path = "/srv/"

execute "apt-get update" do
  command "apt-get update"
end

package "git" do
  # workaround for:
  # WARNING: The following packages cannot be authenticated!
  # liberror-perl
  # STDERR: E: There are problems and -y was used without --force-yes
  options "--force-yes" if node["platform"] == "ubuntu" && node["platform_version"] == "14.04"
end

package 'python3'
package 'python3-pip'
package 'libpq-dev'
package 'libffi-dev'
package 'libcurl4-openssl-dev'
package 'libtiff5-dev'
package 'libjpeg8-dev'
package 'zlib1g-dev'
package 'libfreetype6-dev'
package 'libcurl4-gnutls-dev'
package 'libgnutls28-dev'

execute 'execute_add_remote_host' do
  command "ssh-keyscan github.com >> ~/.ssh/known_hosts"
end

execute 'execute_clear_apps_path' do
  cwd "#{apps_path}/"
  command "rm -rf *"
end
