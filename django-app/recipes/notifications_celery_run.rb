include_recipe "::install_code"

app = search(:aws_opsworks_app).first
app_path = "/srv/#{app['shortname']}"

systemd_file = "[Unit]\n"
systemd_file += "Description=celery_notif daemon\n\n"
systemd_file += "[Service]\n"
systemd_file += "PIDFile=/run/celery/pid\n"
systemd_file += "User=root\n"
systemd_file += "Group=root\n"
systemd_file += "RuntimeDirectory=celery\n"
systemd_file += "WorkingDirectory=#{app_path}/repo\n"
systemd_file += "ExecStart=/usr/local/bin/celery worker -A webapp -Q notifications --pidfile=/run/celery/pid --loglevel=info --autoscale=10,5\n"
systemd_file += "ExecReload=/bin/kill -s HUP $MAINPID\n"
systemd_file += "ExecStop=/bin/kill -s TERM $MAINPID\n"
systemd_file += "StandardOutput=syslog\n"
systemd_file += "StandardError=syslog\n"
systemd_file += "SyslogIdentifier=celery_notif-service\n"
systemd_file += "Restart=always\n\n"
systemd_file += "[Install]\n"
systemd_file += "WantedBy=multi-user.target"

file "/lib/systemd/system/celery_notif.service" do
  content systemd_file
  mode '0644'
  owner 'root'
  group 'root'
end

execute 'execute_systemd-reload-settings' do
  command "systemctl daemon-reload"
end

execute 'execute_systemd-enable-celery_notif' do
  command "systemctl enable celery_notif.service"
end

execute 'execute_systemd-start-celery_notif' do
  command "systemctl start celery_notif.service"
end

execute 'execute_systemd-restart-celery_notif' do
  command "systemctl restart celery_notif.service"
end
