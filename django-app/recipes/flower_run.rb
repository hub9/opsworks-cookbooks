app = search(:aws_opsworks_app).first
app_path = "/srv/#{app['shortname']}"

directory app_path do
  owner 'root'
  group 'root'
  mode '0777'
  action :create
end

execute 'execute_requirements' do
  command "pip3 install flower"
end

systemd_file = "[Unit]\n"
systemd_file += "Description=celery flower daemon\n\n"
systemd_file += "[Service]\n"
systemd_file += "PIDFile=/run/flower/pid\n"
systemd_file += "User=root\n"
systemd_file += "Group=root\n"
systemd_file += "RuntimeDirectory=flower\n"
systemd_file += "ExecStart=/usr/local/bin/flower --pidfile=/run/flower/pid --address=0.0.0.0 --port=80 --broker=#{app['environment']['BROKER_URL']}\n"
systemd_file += "ExecReload=/bin/kill -s HUP $MAINPID\n"
systemd_file += "ExecStop=/bin/kill -s TERM $MAINPID\n"
systemd_file += "StandardOutput=syslog\n"
systemd_file += "StandardError=syslog\n"
systemd_file += "SyslogIdentifier=celery-flower-service\n"
systemd_file += "Restart=always\n\n"
systemd_file += "[Install]\n"
systemd_file += "WantedBy=multi-user.target"

file "/lib/systemd/system/flower.service" do
  content systemd_file
  mode '0644'
  owner 'root'
  group 'root'
end

execute 'execute_systemd-reload-settings' do
  command "systemctl daemon-reload"
end

execute 'execute_systemd-enable-flower' do
  command "systemctl enable flower.service"
end

execute 'execute_systemd-start-flower' do
  command "systemctl start flower.service"
end

execute 'execute_systemd-restart-flower' do
  command "systemctl restart flower.service"
end
