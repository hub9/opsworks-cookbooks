name 'django-app'
maintainer 'Hub9'
maintainer_email 'dev@hub9.co'
license 'All Rights Reserved'
description 'Installs/Configures django app'
long_description 'Installs/Configures django app environment'
version '0.1.0'
chef_version '>= 12.1' if respond_to?(:chef_version)


